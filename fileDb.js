const fs = require('fs');
const nanoid = require("nanoid");

let data = null;


module.exports = {
  init: () => {
    return new Promise((resolve, reject) => {
      fs.readFile('./db.json', (err, result) => {
        if (err) {
          reject(err);
        } else {
          data = JSON.parse(result);
          resolve();
        }
      });
    });
  },
  getData: (datetime) => {
    if (datetime) {
      const index = data.findIndex(message => datetime === message.datetime);

      const piecesOfMessages = data.slice(index + 1);
      if (piecesOfMessages.length !== 0) {
        return piecesOfMessages;
      } else {
        return [];
      }

    } else {
      return data.sort().slice(-30);
    }
  },
  addItem: (item) => {
    item.id = nanoid();
    item.datetime = (new Date()).toISOString();
    data.push(item);


    let contents = JSON.stringify(data, null, 2);

    return new Promise((resolve, reject) => {
      fs.writeFile('./db.json', contents, err => {
        if (err) {
          reject(err);
        } else {
          resolve(item);
        }
      });
    });
  }
};