const express = require('express');
const router = express.Router();

const createRouter = (db) => {
  router.get('/', (req, res) => {
    if (req.query.datetime) {
      res.send(db.getData(req.query.datetime));
    } else {
      res.send(db.getData());
    }
  });

  router.post('/', (req, res) => {
    if (req.body.author && req.body.message) {
      const message = req.body;


      db.addItem(message).then(result => {
        res.send(result);
      });
    } else {
      res.status(400).send({"error 400": "Author and message must be present in the request"});
    }

  });

  return router;
};

module.exports = createRouter;